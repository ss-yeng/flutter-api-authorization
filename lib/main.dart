// import 'package:akat_app/ui/pages/forget/change.password.dart';
// import 'package:akat_app/ui/pages/home/home.page.dart';
// import 'package:akat_app/ui/pages/language/lao.language.dart';
// import 'package:akat_app/ui/pages/signin/signin.page.dart';
// import 'package:flutter/material.dart';

// void main() {
//   runApp(const MyApp());
// }

// class MyApp extends StatelessWidget {
//   const MyApp({Key? key}) : super(key: key);

//   // This widget is the root of your application.
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: const HomePage(),
//       debugShowCheckedModeBanner: false,
//     );
//   }
// }

import 'dart:ui';
import 'package:akat_app/ui/pages/home/home.page.dart';
import 'package:easy_localization/easy_localization.dart'; // import custom loaders
import 'package:easy_localization_loader/easy_localization_loader.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  // String? translationsPath = "";
  runApp(EasyLocalization(
    child: MyApp(),
    // ignore: prefer_const_literals_to_create_immutables
    supportedLocales: [
      const Locale('en', 'US'),
      const Locale('ar', 'DZ'),
      const Locale('de', 'DE'),
      const Locale('ru', 'RU')
    ],
    path: 'assets/translations/',
    // fallbackLocale: Locale('en', 'US'),
    // startLocale: Locale('de', 'DE'),
    // saveLocale: false,
    // useOnlyLangCode: true,

    // optional assetLoader default used is RootBundleAssetLoader which uses flutter's assetloader
    // install easy_localization_loader for enable custom loaders
    // assetLoader: RootBundleAssetLoader()
    // assetLoader: HttpAssetLoader().load(translationsPath, const Locale('en'))
    // assetLoader: FileAssetLoader()
    // assetLoader: CsvAssetLoader()
    // assetLoader: YamlAssetLoader() //multiple files
    // assetLoader: YamlSingleAssetLoader() //single file
    // assetLoader: XmlAssetLoader() //multiple files
    // assetLoader: XmlSingleAssetLoader() //single file
    // assetLoader: CodegenLoader()
  ));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: const HomePage(),
    );
  }
}
