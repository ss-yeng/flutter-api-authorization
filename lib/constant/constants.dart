import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

const String firstUrl = 'apps.akat.la:7291';
const String nextUrl = 'api/';

const String famillyFont = "NotoSansLao";

const String token =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiIwNzhmYTMwNC1hMDQ3LTRkNmEtOWM3ZC05ZWM0M2M3NDk0ZTMiLCJpc3MiOiJodHRwOi8vYWthdC5sYS8iLCJpYXQiOjE2MzM1MjQyNTIsImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMDUvMDUvaWRlbnRpdHkvY2xhaW1zL25hbWVpZGVudGlmaWVyIjoiMTAwMzAiLCJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1lIjoiMTAwMzAiLCJEaXNwbGF5TmFtZSI6IjEwMDMwIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9zZXJpYWxudW1iZXIiOiJzb3VuNmQiLCJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3VzZXJkYXRhIjoiMTAwMzAiLCJyb2xlaWQiOiI5OTk5OSIsInVzZXJpZCI6IjEwMDMwIiwiYWNjZXNzdHlwZSI6IjAiLCJuYmYiOjE2MzM1MjQyNTIsImV4cCI6MTYzMzU2ODY1MiwiYXVkIjoiQW55In0.-jDJINwRjAZNy4JuJ3DA4AevkDTj1IuywxBt3z-b7Fk";

void alertWidget({
  BuildContext? context,
  final String? title,
  final String? desc,
  final String? subtitle,
  final AlertType? type,
  final VoidCallback? onPressed,
}) {
  Alert(
      context: context!,
      style: const AlertStyle(
        titleStyle: TextStyle(
            fontFamily: famillyFont, fontSize: 14, fontWeight: FontWeight.bold),
        descStyle: TextStyle(
          fontFamily: famillyFont,
          fontSize: 12,
        ),
      ),
      title: title,
      desc: desc,
      type: type,
      buttons: [
        DialogButton(
          color: Colors.black,
          child: Text(
            subtitle!,
            style: const TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontFamily: famillyFont,
            ),
          ),
          onPressed: () {
            if (onPressed != null) {
              onPressed();
            }
          },
          width: 120,
        ),
      ]).show();
}
