import 'package:akat_app/constant/constants.dart';
import 'package:flutter/material.dart';

class ChangePasswordPage extends StatefulWidget {
  const ChangePasswordPage({Key? key}) : super(key: key);

  @override
  _ChangePasswordPageState createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage> {
  Key? key;
  final formKey = GlobalKey<FormState>();
  TextEditingController? userNameController = TextEditingController();
  TextEditingController? oldPasswordController = TextEditingController();
  TextEditingController? newPasswordController = TextEditingController();
  TextEditingController? confirmNewPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("ChangePassword"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Form(
              key: formKey,
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    children: [
                      TextFormField(
                        controller: userNameController,
                        keyboardType: TextInputType.text,
                        decoration: const InputDecoration(
                          errorStyle: TextStyle(
                            fontFamily: famillyFont,
                            fontSize: 14,
                          ),
                          hintText: "ຊື່ຜູ້ໃຊ້",
                          hintStyle: TextStyle(
                            fontSize: 14,
                            fontFamily: famillyFont,
                          ),
                          labelText: "ຊື່ຜູ້ໃຊ້",
                          labelStyle: TextStyle(
                            fontSize: 14,
                            fontFamily: famillyFont,
                          ),
                        ),
                        //ກວດສອບກໍ່ລະນີຄ່າ ວ່າງເປົ່າ
                        validator: (val) {
                          if (val!.isEmpty) {
                            return "ກະລຸນາປ້ອນຊື່ຜູ້ໃຊ້";
                          }
                          return null;
                        },
                      ),
                      TextFormField(
                        controller: oldPasswordController,
                        keyboardType: TextInputType.text,
                        obscureText: true,
                        decoration: const InputDecoration(
                            hintText: "ລະຫັດຜ່ານເກົ່າ",
                            hintStyle: TextStyle(
                              fontSize: 14,
                              fontFamily: famillyFont,
                            ),
                            labelText: "ລະຫັດຜ່ານເກົ່າ",
                            labelStyle: TextStyle(
                              fontSize: 14,
                              fontFamily: famillyFont,
                            ),
                            errorStyle: TextStyle(
                              fontFamily: famillyFont,
                              fontSize: 14,
                            )),
                        validator: (val) {
                          if (val!.isEmpty) {
                            return "ກະລຸນາປ້ອນລະຫັດຜ່ານເກົ່າ";
                          } else if (val.length < 6) {
                            return "ລະຫັດຜ່ານ ສັ້ນເກີນໄປ";
                          }
                          return null;
                        },
                      ),
                      TextFormField(
                        controller: newPasswordController,
                        keyboardType: TextInputType.text,
                        obscureText: true,
                        decoration: const InputDecoration(
                            hintText: "ລະຫັດຜ່ານໃໝ່",
                            hintStyle: TextStyle(
                              fontSize: 14,
                              fontFamily: famillyFont,
                            ),
                            labelText: "ລະຫັດຜ່ານໃໝ່",
                            labelStyle: TextStyle(
                              fontSize: 14,
                              fontFamily: famillyFont,
                            ),
                            errorStyle: TextStyle(
                              fontFamily: famillyFont,
                              fontSize: 14,
                            )),
                        validator: (val) {
                          if (val!.isEmpty) {
                            return "ກະລຸນາປ້ອນລະຫັດຜ່ານໃໝ່";
                          } else if (val.length < 6) {
                            return "ລະຫັດຜ່ານ ສັ້ນເກີນໄປ";
                          }
                          return null;
                        },
                      ),
                      TextFormField(
                        controller: confirmNewPasswordController,
                        keyboardType: TextInputType.text,
                        obscureText: true,
                        decoration: const InputDecoration(
                            hintText: "ຢືນຢັນລະຫັດຜ່ານໃໝ່",
                            hintStyle: TextStyle(
                              fontSize: 14,
                              fontFamily: famillyFont,
                            ),
                            labelText: "ຢືນຢັນລະຫັດຜ່ານໃໝ່",
                            labelStyle: TextStyle(
                              fontSize: 14,
                              fontFamily: famillyFont,
                            ),
                            errorStyle: TextStyle(
                              fontFamily: famillyFont,
                              fontSize: 14,
                            )),
                        validator: (val) {
                          if (val!.isEmpty) {
                            return "ກະລຸນາປ້ອນ ຢືນຢັນລະຫັດຜ່ານໃໝ່";
                          } else if (val.length < 6) {
                            return "ລະຫັດຜ່ານ ສັ້ນເກີນໄປ";
                            // ignore: unrelated_type_equality_checks
                          } else if (confirmNewPasswordController != val) {
                            return "ລະຫັດຜ່ານທັງສອງບໍຕົງກັນ";
                          }
                          return null;
                        },
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      SizedBox(
                        height: 50,
                        width: MediaQuery.of(context).size.width,
                        // ignore: deprecated_member_use
                        child: RaisedButton(
                          onPressed: () {
                            if (formKey.currentState!.validate()) {}
                          },
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(
                            10,
                          )),
                          child: Text(
                            "ຢືນຢັນ".toUpperCase(),
                            style: const TextStyle(
                              fontSize: 24,
                              color: Colors.white,
                              fontFamily: famillyFont,
                            ),
                          ),
                          color: Colors.green,
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
