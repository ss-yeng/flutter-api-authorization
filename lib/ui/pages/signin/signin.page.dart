import 'dart:convert';

import 'package:akat_app/constant/constants.dart';
import 'package:akat_app/ui/pages/home/home.page.dart';
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class SigninPage extends StatefulWidget {
  const SigninPage({Key? key}) : super(key: key);

  @override
  _SigninPageState createState() => _SigninPageState();
}

class _SigninPageState extends State<SigninPage> {
  Key? key;
  final formKey = GlobalKey<FormState>();
  TextEditingController? userNameController = TextEditingController();
  TextEditingController? passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(top: 100, left: 10, right: 10),
          child: Column(
            children: [
              const Text(
                "Sign in",
                style: TextStyle(
                  fontSize: 32,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Form(
                key: formKey,
                child: Card(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 10),
                    child: Column(
                      children: [
                        TextFormField(
                          controller: userNameController,
                          keyboardType: TextInputType.text,
                          decoration: const InputDecoration(
                            hintText: "ຊື່ຜູ້ໃຊ້",
                            hintStyle: TextStyle(
                              fontSize: 14,
                              fontFamily: famillyFont,
                            ),
                            labelText: "ຊື່ຜູ້ໃຊ້",
                            labelStyle: TextStyle(
                              fontSize: 14,
                              fontFamily: famillyFont,
                            ),
                          ),
                          //ກວດສອບກໍ່ລະນີຄ່າ ວ່າງເປົ່າ
                          validator: (val) {
                            if (val!.isEmpty) {
                              return "ກະລຸນາປ້ອນຊື່ຜູ້ໃຊ້";
                            }
                            return null;
                          },
                        ),
                        TextFormField(
                          controller: passwordController,
                          keyboardType: TextInputType.text,
                          obscureText: true,
                          decoration: const InputDecoration(
                              hintText: "ລະຫັດຜ່ານ",
                              hintStyle: TextStyle(
                                fontSize: 14,
                                fontFamily: famillyFont,
                              ),
                              labelText: "ລະຫັດຜ່ານ",
                              labelStyle: TextStyle(
                                fontSize: 14,
                                fontFamily: famillyFont,
                              ),
                              errorStyle: TextStyle(
                                fontFamily: famillyFont,
                                fontSize: 14,
                              )),
                          validator: (val) {
                            if (val!.isEmpty) {
                              return "ກະລຸນາປ້ອນລະຫັດຜ່ານ";
                            } else if (val.length < 6) {
                              return "ລະຫັດຜ່ານ ສັ້ນເກີນໄປ";
                            }
                            return null;
                          },
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        SizedBox(
                          height: 50,
                          width: MediaQuery.of(context).size.width,
                          // ignore: deprecated_member_use
                          child: RaisedButton(
                            onPressed: () {
                              if (formKey.currentState!.validate()) {
                                signIn(
                                    username: userNameController!.text,
                                    password: passwordController!.text);
                              }
                            },
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(
                              10,
                            )),
                            child: Text(
                              "Login".toUpperCase(),
                              style: const TextStyle(
                                fontSize: 24,
                                color: Colors.white,
                              ),
                            ),
                            color: Colors.green,
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        )
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

// ຟັນຊັນ ສົ່ງຂໍ້ມູນໃນການ ເຂົ້າສູ່ລະບົບ ດ້ວຍ username and password
  Future signIn({String? username, String? password}) async {
    // ເກັບເປັນ local storage
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Map data = {
      'username': username!,
      'password': password!,
    };

    var jsonResponse;
    var response = await http.post(
      Uri.http(firstUrl, nextUrl + "User/signIn"),
      body: data,
      // headers: {
      //   "Content-Type": "application/json",
      //   "Accept": "application/json"
      // },
    );

    print(response.body);

    if (response.statusCode == 200) {
      print("New Yeng: $data");
      //ກໍລະນີຖືກຕ້ອງ
      jsonResponse = json.decode(response.body);
      print("\n\n jsonResponse: ");
      print(jsonResponse);
      if (jsonResponse == null) {
        Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => HomePage(
                      key: key,
                    )));
        //ໃຫ້ເກັບຄ່າ access toke, username, refresh token ຢູ່ໃນ local storage
        sharedPreferences.setString(
            "access_token", jsonResponse['access_token'].toString());
        sharedPreferences.setString("username", jsonResponse['username']);
        sharedPreferences.setString(
            "refresh_token", jsonResponse['refresh_token']);
      }
    } else {
      //ກໍລະນີບໍຖືກຕ້ອງ
      alertWidget(
          context: context,
          title: "ຜິດພາດ",
          desc: "ຊື່ຜູ້ໃຊ້ ແລະ ລະຫັດຜ່ານບໍ່ຖືກຕ້ອງ",
          type: AlertType.error,
          subtitle: "ຕົກລົງ",
          onPressed: () {
            Navigator.pop(context);
          });
    }
  }
}
