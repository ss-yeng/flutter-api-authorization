import 'package:akat_app/constant/constants.dart';
import 'package:akat_app/ui/pages/forget/change.password.dart';
import 'package:akat_app/ui/pages/language/en.language.dart';
import 'package:akat_app/ui/pages/language/home.translate.page.dart';
import 'package:akat_app/ui/pages/language/key.page.dart';
import 'package:akat_app/ui/pages/language/lao.language.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Key? key;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Home page"),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Center(
              child: Text(
                "ຊື່",
                style: TextStyle(
                  fontFamily: famillyFont,
                  fontSize: 24,
                ),
              ),
            ),
            const Text(
              "10030",
              style: TextStyle(
                fontFamily: famillyFont,
                fontSize: 18,
                fontWeight: FontWeight.bold,
              ),
            ),
            TextButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ValueKeyTextPage(
                              key: key,
                            )));
              },
              child: const Text(
                "Key",
                style: TextStyle(
                  fontFamily: famillyFont,
                ),
              ),
            ),
            TextButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => LaoLanguage(
                              key: key,
                            )));
              },
              child: const Text(
                "ພາສາລາວ",
                style: TextStyle(
                  fontFamily: famillyFont,
                ),
              ),
            ),
            TextButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => EnLanguage(
                              key: key,
                            )));
              },
              child: const Text(
                "ພາສາອັງກິດ",
                style: TextStyle(
                  fontFamily: famillyFont,
                ),
              ),
            ),
            TextButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => MyHomePage(
                              key: key,
                            )));
              },
              child: const Text(
                "ແປພາສາ",
                style: TextStyle(
                  fontFamily: famillyFont,
                  color: Colors.green,
                  fontSize: 24,
                ),
              ),
            ),
            TextButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ChangePasswordPage(
                              key: key,
                            )));
              },
              child: const Text(
                "ປ່ຽນລະຫັດຜ່ານ",
                style: TextStyle(
                  fontFamily: famillyFont,
                ),
              ),
            ),
            TextButton(
              onPressed: () {},
              child: const Text(
                "ອອກຈາກລະບົບ",
                style: TextStyle(
                  fontFamily: famillyFont,
                  color: Colors.red,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
