import 'dart:convert';
import 'package:akat_app/constant/constants.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class ValueKeyTextPage extends StatefulWidget {
  const ValueKeyTextPage({Key? key}) : super(key: key);

  @override
  _ValueKeyTextPageState createState() => _ValueKeyTextPageState();
}

class _ValueKeyTextPageState extends State<ValueKeyTextPage> {
  Future getData() async {
    String url =
        'http://apps.akat.la:7291/api/Translation/listTranslationByPlatform/2';
    var res = await http.get(Uri.parse(url));
    // var res = await http.get(Uri.http('http://apps.akat.la:7291',
    //     'api/Translation/listTranslationByPlatform/2?fbclid=IwAR39s7Svich05qO9fD0BMEYq2h-sKdHJFOQqFbdnopOpqP5eFVg7umepFUA'));
    return jsonDecode(res.body.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Key",
          style: TextStyle(
            fontFamily: famillyFont,
          ),
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(0),
        child: FutureBuilder(
          future: getData(),
          builder: (context, AsyncSnapshot? snapshot) {
            if (snapshot!.hasData) {
              // print('data:   ${snapshot.data}');
              final newData = snapshot.data['model'];
              // print(snapshot.data['model']);
              // print(snapshot.data['model'].length);
              return ListView.builder(
                  itemCount: newData.length,
                  itemBuilder: (context, index) {
                    return Card(
                      child: Center(
                        child: Text(
                          newData[index]['key_Text'],
                          style: const TextStyle(
                            fontFamily: famillyFont,
                          ),
                        ),
                      ),
                    );
                  });
            } else if (snapshot.hasError) {
              return const Center(
                child: Text('No data'),
              );
            }
            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }
}
