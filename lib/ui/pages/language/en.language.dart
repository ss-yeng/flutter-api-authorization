import 'dart:convert';
import 'package:akat_app/constant/constants.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class EnLanguage extends StatefulWidget {
  const EnLanguage({Key? key}) : super(key: key);

  @override
  _EnLanguageState createState() => _EnLanguageState();
}

class _EnLanguageState extends State<EnLanguage> {
  Future getData() async {
    String url =
        'http://apps.akat.la:7291/api/Translation/listTranslationByPlatform/2?fbclid=IwAR39s7Svich05qO9fD0BMEYq2h-sKdHJFOQqFbdnopOpqP5eFVg7umepFUA';
    var res = await http.get(Uri.parse(url));
    // var res = await http.get(Uri.http('http://apps.akat.la:7291',
    //     'api/Translation/listTranslationByPlatform/2?fbclid=IwAR39s7Svich05qO9fD0BMEYq2h-sKdHJFOQqFbdnopOpqP5eFVg7umepFUA'));
    return jsonDecode(res.body.toString());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "ພາສາອັງກິດ",
          style: TextStyle(
            fontFamily: famillyFont,
          ),
        ),
      ),
      body: Padding(
        padding:const EdgeInsets.all(0),
        child: FutureBuilder(
          future: getData(),
          builder: (context, AsyncSnapshot? snapshot) {
            if (snapshot!.hasData) {
              // print('data:   ${snapshot.data}');
              final newData = snapshot.data['model'];
              // print(snapshot.data['model']);
              // print(snapshot.data['model'].length);
              return ListView.builder(
                  itemCount: newData.length,
                  itemBuilder: (context, index) {
                    return Card(
                      child: Center(
                        child: Text(newData[index]['textEng']),
                      ),
                    );
                  });
            } else if (snapshot.hasError) {
              return const Center(
                child: Text('No data'),
              );
            }
            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        ),
      ),
    );
  }
}
